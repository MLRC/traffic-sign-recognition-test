#include "trafficrecognitiontest.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/dnn/dnn.hpp"
#include <QImage>
#include <QFile>
#include <iostream>
#include <QDebug>

TrafficRecognitionTest::TrafficRecognitionTest()
{
}

void TrafficRecognitionTest::process(const QString &path, const QString &modelPath,
                                     const QString &signnamesPath)
{
    cv::Mat mat = cv::imread(path.toStdString());

    auto model = cv::dnn::readNetFromONNX(modelPath.toStdString());

    auto signNames = readSignNames(signnamesPath);
    qDebug() << signNames;

    cv::Mat small;
    cv::resize(mat, small, cv::Size(32, 32), cv::INTER_LINEAR);
    cv::cvtColor(small, small, cv::COLOR_BGR2GRAY);

    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    clahe->setClipLimit(40);

    cv::Mat processed;
    clahe->apply(small, processed);
    cv::cvtColor(processed, processed, cv::COLOR_GRAY2RGB);
    std::cout << processed.channels() << " " << processed.size << std::endl;

    cv::Mat normalized;
    processed.convertTo(normalized, CV_32F, 1.0 / 255.0, 0);
    std::cout << normalized << std::endl;

    std::vector<int> dims = {1, 32, 32, 3};
    normalized = normalized.reshape(1, dims);
    std::cout << normalized.dims << " >>>> " << normalized.size << std::endl;
    model.setInput(normalized);
    cv::Mat res = model.forward();
    double maxValue = -1;
    double minValue = -1;
    int maxIdx[2];
    int minIdx[2];
    cv::minMaxIdx(res, &minValue, &maxValue, minIdx, maxIdx);
    std::cout << res << std::endl;
    qDebug() << maxIdx[1] << signNames.value(maxIdx[1]) << maxValue;
}

QStringList TrafficRecognitionTest::readSignNames(const QString &path)
{
    QStringList names;
    QFile file(path);
    if (file.open(QFile::ReadOnly))
    {
        auto contents = file.readAll().split('\n');
        for (int i = 1; i < contents.size(); ++i)
        {
            auto lineData = contents.value(i).split(',');
            if (lineData.size() > 1)
            {
                names.append(lineData.value(1));
            }
        }
    }
    return names;
}


//	model2.setInput(image)
//	preds2 = model2.forward()
//	print(imagePath, preds)
//#	preds2 = model.predict(image)
//	j2 = preds2.argmax(axis=1)[0]
//	label2 = labelNames[j2]
//	print(i, label, label2, j, j2)

//	# load the image using OpenCV, resize it, and draw the label
//	# on it
//	image = cv2.imread(imagePath)
//	image = imutils.resize(image, width=128)
//	cv2.putText(image, label, (5, 15), cv2.FONT_HERSHEY_SIMPLEX,
//		0.45, (0, 0, 255), 2)

//	# save the image to disk
//	p = os.path.sep.join([args["examples"], "{}.png".format(i)])
//	cv2.imwrite(p, image)
