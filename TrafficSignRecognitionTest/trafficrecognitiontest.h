#ifndef TRAFFICRECOGNITIONTEST_H
#define TRAFFICRECOGNITIONTEST_H

#include <QString>

class TrafficRecognitionTest
{
public:
    TrafficRecognitionTest();

    void process(const QString &path, const QString &modelPath, const QString &signnamesPath);
private:
    static QStringList readSignNames(const QString &path);
};

#endif // TRAFFICRECOGNITIONTEST_H
