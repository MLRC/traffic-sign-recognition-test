#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStandardPaths>
#include <QDir>
#include "trafficrecognitiontest.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QString dataDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/";
    QString assetsPath(QStringLiteral("assets:/"));
    QString onnxModel(QStringLiteral("trafficsignnet.onnx"));
    QFile onnxFile(assetsPath + onnxModel);
    onnxFile.copy(dataDir + onnxModel);

    QString imagePath(QStringLiteral("00008.png"));
    QFile imageFile(assetsPath + imagePath);
    imageFile.copy(dataDir + imagePath);

    QString signNames(QStringLiteral("signnames.csv"));
    QFile signnamesFile(assetsPath + signNames);
    signnamesFile.copy(dataDir + signNames);

    TrafficRecognitionTest test;
    test.process(dataDir + imagePath, dataDir + onnxModel, dataDir + signNames);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
