android {
    # full path to OpenCV Android SDK
    PLATFORM_NAME = armeabi-v7a

    OPENCV_PATH = $$PWD/android

    INCLUDEPATH += $${OPENCV_PATH}/native/jni/include/

    LIBS += -L$${OPENCV_PATH}/native/libs/$${PLATFORM_NAME} -lopencv_java4

    ANDROID_EXTRA_LIBS += $${OPENCV_PATH}/native/libs/$${PLATFORM_NAME}/libopencv_java4.so
}
